#ifndef SERVER_H
#define SERVER_H
// This needs to be at the top apparently
#define _POSIX_C_SOURCE 200112L
#define _GNU_SOURCE

#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h>
#include <strings.h>
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <time.h>
#include <stdbool.h>

#include "leaderboard.h"

#define ARRAY_SIZE 30  /* Size of array to receive */
#define BACKLOG 10       /* Backlog of connections allowed */
#define NUM_HANDLER_THREADS 10
#define RETURNED_ERROR -1
#define MAXDATASIZE 100 /* max number of bytes we can get at once */
#define RANDOM_NUMBER_SEED 42
#define NUM_TILES_X 9 + 2
#define NUM_TILES_Y 9 + 2
#define NUM_MINES 10
#define CURSOR_OFFSET 2

/* STRUCT DEFINITIONS */
/* Information for each tile */
typedef struct
{
  char symbol; /* '-' = unknown, '*' = mine, 'o' = no mine */
  int adjacent_mines;
  bool revealed;
  bool is_mine;
  char x_coord;
  int y_coord;
  bool exists; 			/* False . This will be for padding the array for boundary conditions */
} Tile;

/* store game state in a struct */
typedef struct GameState
{
  // ... additional fields ...
  Tile tiles[NUM_TILES_X][NUM_TILES_Y]; // 2D array of tiles, each with adj, revealed?, is_mine? members
  int mines_remaining;
  bool running;
  int cursor_x;
  int cursor_y;
  int mine_counter;
} Mine_Yard;


typedef struct Player_S {
  int wins;
  bool online;
  char name[20];
  int  games_played;
} PLAYER_T;


/* format of a single request. */
struct request {
    int socket_id;             /* number of the request                  */
    struct request* next;   /* pointer to next request, NULL if none. */
};

struct request* requests = NULL;     /* head of linked list of requests. */
struct request* last_request = NULL; /* pointer to last request.         */

/* Global Variables */ 
int n;
char buffer[256];
char ans = 'n';
LIST_NODE_T *lboard = NULL;
int num_requests = 0;

/* global mutex for our program. assignment initializes it. */
/* note that we use a RECURSIVE mutex, since a handler      */
/* thread might try to lock it twice consecutively.         */
pthread_mutex_t request_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_cond_t  got_request   = PTHREAD_COND_INITIALIZER;
pthread_t  p_threads[NUM_HANDLER_THREADS];
int thr_id[NUM_HANDLER_THREADS];
int connected_sockets[NUM_HANDLER_THREADS];
int sockfd;

/* Function Declarations */
/* A function declaration has the following parts − */
/* return_type function_name( parameter list ); */
void INThandler(int sig);
void add_request(int request_num, pthread_mutex_t* p_mutex, pthread_cond_t*  p_cond_var);
void handle_request(int socket_id);
void* handle_requests_loop(void* data);
int send_lb(LIST_NODE_T *head, int socket_id);
void Send_Array_Data_Int(int socket_id, int *myArray);
void SendLeaderBoard(int socket_id);
void SendMineYard(int socket_id, Mine_Yard *game);
void init_adjacent_mines (Mine_Yard *minesweeper);
void init_adjacent_mines (Mine_Yard *minesweeper);
void clear_tiles(Mine_Yard *minesweeper);
void init_tiles(Mine_Yard *minesweeper);
void add_mines(Mine_Yard *minesweeper);
void change_symbol(int x, int y, Mine_Yard *game);
void reveal(int x, int y, Mine_Yard *game);
int reveal_tile(int socket_id, Mine_Yard *game);
int place_flag(int socket_id, Mine_Yard *game);
void play_minesweeper(int socket_id, PLAYER_T *Player);
int *Receive_Array_Int_Data(int socket_identifier, int size);
void Client_Request(int socket_id,PLAYER_T Player);
void *Handle_Request(void *newsock);

#endif /* SERVER_H */
